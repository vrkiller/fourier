// reference path used in visual studio code for code hints. Can be deleted.
/// <reference path="C:\Users\Victor's monster\Desktop\programming\fourierLetters\bin_p5/p5.js"/>
/// <reference path="C:\Users\Victor's monster\Desktop\programming\fourierLetters\bin_p5/p5.min.js"/>
/// <reference path="C:\Users\Victor's monster\Desktop\programming\fourierLetters\bin_p5/p5.pre-min.js"/>

/// <reference path="C:\Users\Victor's monster\Desktop\programming\fourierLetters\bin_p5/p5.d.ts"/>
/// <reference path="C:\Users\Victor's monster\Desktop\programming\fourierLetters\bin_p5/p5.global-mode.d.ts"/>

// time in seconds
let time = 0;
let wave = []
let trace =  []
let fpsMax = 60;


var uiRelativePos = null;
// ui objects:
var sliderTimeMultiplier = null;
var sliderN = null;

var checkkBoxShowTrace = null;

function setup() {
    
    frameRate(fpsMax);
    setFrameRate(setFrameRate);
    createCanvas(1900, 930);

    // create ui
    uiRelativePos = createVector(100,screen.height-400);
    console.log(uiRelativePos.toString())
    sliderTimeMultiplier = createSlider(0,2,1,0.02);
    sliderN = createSlider(0,300,1,2);
    checkkBoxShowTrace = createCheckbox("",true);

    sliderTimeMultiplier.position(uiRelativePos.x,uiRelativePos.y)
    sliderN.position(uiRelativePos.x,uiRelativePos.y+30)
    checkkBoxShowTrace.position(uiRelativePos.x,uiRelativePos.y+60)


  }
  
function draw() {
    background(0);

    // update ui
    {

        color(250);
        textSize(10);
        
        text(sliderTimeMultiplier.value()+"x seconds", sliderTimeMultiplier.x + sliderTimeMultiplier.width + 10, sliderTimeMultiplier.y+15);
        text(sliderN.value(), sliderN.x + sliderN.width + 10, sliderN.y+15 );
        text("Show Trace", checkkBoxShowTrace.x+20, checkkBoxShowTrace.y+13 );
    }
    //console.log(checkkBoxShowTrace.x);

    
    var lastx = 200, lasty = 200;

    for(n=1; n<sliderN.value(); n+=2){

        let radius = 70*(4/(n*PI));

        // Circle
        fill(204, 101, 192, 127);
        noFill();
        strokeWeight(1)
        stroke(255, 255, 255);
        ellipse(lastx, lasty, radius*2);
        
        // radial line
        let xb = lastx - radius * cos(-time*n*TWO_PI-PI);
        let yb = lasty + radius * sin(-time*n*TWO_PI);
        line(lastx,lasty,xb,yb);

        lastx = xb;
        lasty = yb;
        lastn = n;
    }
    
    console.log(( frameRate() * 1/sliderTimeMultiplier.value() ))
    //draw trace
    while( trace.length > ( fpsMax* 1/sliderTimeMultiplier.value())/4 ){
        trace.pop();

    }

    if(checkkBoxShowTrace.checked()){
        
        trace.unshift(createVector(lastx,lasty));

        beginShape();
        for(let ind=0; ind<trace.length; ind+=1){
            stroke(250)
            vertex(trace[ind].x,trace[ind].y);

            strokeWeight(3)
            stroke(255,0,0);
            point(trace[ind].x,trace[ind].y);
            strokeWeight(1)
        }
        endShape();
    }



    // draw oscillograph:
    stroke(100)
    line(lastx,lasty,400,lasty)
    stroke(250)
    wave.unshift(createVector(3*time*140*(4/PI),lasty));
    stroke(255);
    strokeWeight(1)
    beginShape();
    for(let ind=0; ind<wave.length; ind+=1){

        let x = 400+wave[0].x - wave[ind].x;
        let y = wave[ind].y
    
        vertex(x,y)
        //point(x, y);


    }
    while(wave.length>400){
        wave.pop();
    }
    endShape();

    //console.log("framerate: ",time);
    if(frameRate()!= 0){
        time+=sliderTimeMultiplier.value()*(1/frameRate());
    }
    
    
}
